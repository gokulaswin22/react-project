function validate() {
    var email = document.getElementById('email').value;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var username = document.getElementById("username").value;
    var mob_regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("conformpassword").value;

    if (username == null || username == "") {
        alert("Please enter the username.");
        return false;
    }

    
    if (!filter.test(email.value)) {
        alert('Please provide a valid email address');
        email.focus;
        return false;
    }


    if (password == "") {
        window.alert("Please enter your password");
        password.focus();
        return false;
    }

    if (confirmPassword == "") {
        window.alert("Please enter to confirm your password");
        confirmPassword.focus();
        return false;
    }

    if (password != confirmPassword) {
        alert("Entered Password is not matching!! Try Again");
        return false;
    }

    alert('Login successful');
}

